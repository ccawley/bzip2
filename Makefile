COMPONENT ?= bzip2
CDEFINES   = -DBZ_DEBUG=0 -DBZ_STRICT_ANSI
OBJS       = blocksort bzlib compress crctable decompress huffman randtable
HDRS       = bzlib

ifeq (${COMPONENT},bz2lib)
include CLibrary
else
OBJS      += ${COMPONENT}
include CApp
endif

# Dynamic dependencies:
